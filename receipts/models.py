# This code up here tells django where to import models
# and anything else you need to shape them
from django.db import models
from django.conf import settings


# Create your models here.


# Models are created as classes
class ExpenseCategory(models.Model):
    # These are the variable names for the attributes
    name = models.CharField(max_length=50)
    # A foreign key is a one to many relationship
    # Here one user can have many expenses
    owner = models.ForeignKey(
        # Here we use the authorized user model imported
        # from settings
        settings.AUTH_USER_MODEL,
        # A related name that will be used in the urls.py
        # to link this model
        related_name="categories",
        # If the owner is deleted, all his expenses
        # are deleted as well with CASCADE
        on_delete=models.CASCADE,
    )


class Account(models.Model):
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="accounts",
        on_delete=models.CASCADE,
    )


class Receipt(models.Model):
    vendor = models.CharField(max_length=200)
    total = models.DecimalField(("Total $"), max_digits=10, decimal_places=3)
    tax = models.DecimalField(("Tax $"), max_digits=10, decimal_places=3)
    date = models.DateTimeField(("Date"), auto_now=False, auto_now_add=False)
    purchaser = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="receipts",
        on_delete=models.CASCADE,
    )
    category = models.ForeignKey(
        ExpenseCategory,
        related_name="receipts",
        on_delete=models.CASCADE,
    )
    account = models.ForeignKey(
        Account,
        related_name="receipts",
        on_delete=models.CASCADE,
        null=True,
    )
